<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Cartman Tour" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="ExampleDialog" src="behavior_1/ExampleDialog/ExampleDialog.dlg" />
    </Dialogs>
    <Resources>
        <File name="styles" src="html/css/styles.css" />
        <File name="StopSign" src="html/images/StopSign.png" />
        <File name="index" src="html/index.html" />
        <File name="jquery-3.2.1.min" src="html/js/jquery-3.2.1.min.js" />
        <File name="cartman" src="html/js/cartman.js" />
        <File name="cartman_script" src="data/cartman_script.txt" />
        <File name="harvey_script" src="data/harvey_script.txt" />
        <File name="index - Copy" src="html/index - Copy.html" />
        <File name="loofer" src="html/images/loofer.png" />
        <File name="marbles" src="html/images/marbles.png" />
        <File name="sponges" src="html/images/sponges.png" />
        <File name="pet_bowl" src="html/images/pet_bowl.png" />
        <File name="socks" src="html/images/socks.png" />
        <File name="speedstick" src="html/images/speedstick.png" />
    </Resources>
    <Topics>
        <Topic name="ExampleDialog_enu" src="behavior_1/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
    </Topics>
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
