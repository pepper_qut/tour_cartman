/* globals $, QiSession */

var behaviourManager = false
var memoryService = false

var clickEventType = ((document.ontouchstart !== null) ? 'click' : 'touchstart')

$(document).ready(function () {
  new QiSession(function (session) {
    connected(session)
  }, disconnected)

  $('.exit').on(clickEventType, function () {
    memoryManager.raiseEvent('finish_picking', true)
  })
  $('.item').on(clickEventType, function () {
    $('#loading').show()
    $('#content').hide()

    var item_id = $(this).attr('data-item-id')
    memoryManager.raiseEvent('pick_item', item_id)
    // $('#content').show()
    // $('#loading').hide()
    // $.ajax({
    //   url: 'http://192.168.0.243:8080',
    //   type: 'POST',
    //   data: JSON.stringify({item_id: item_id}),
    //   dataType: 'json',
    //   success: function (data) {
    //
    //     $('#content').show()
    //   }
    // })

  })
})

function connected (session) {
  console.log('Connected')

  session.service('ALBehaviorManager').then(function (service) {
    behaviourManager = service
  })
  session.service('ALMemory').then(function (service) {
    memoryManager = service
    memoryManager.subscriber('pick_complete').then(function (subscriber) {

      subscriber.signal.connect(function (state) {
        $('#loading').hide()
        $('#content').show()
      });
  });
  })
}

function disconnected () {
  console.log('Disconnected...')
}
