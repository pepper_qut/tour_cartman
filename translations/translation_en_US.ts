<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Cartman Tour/Say</name>
        <message>
            <source>Let's go and see Cartman</source>
            <comment>Text</comment>
            <translation type="obsolete">Let's go and see Cartman</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cartman Tour/Say (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>If you want to see cartman in action, select an item on my screen</source>
            <comment>Text</comment>
            <translation type="obsolete">If you want to see cartman in action, select an item on my screen</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cartman Tour/Say (3)</name>
        <message>
            <source>This is embarassing. I seem to be a bit lost</source>
            <comment>Text</comment>
            <translation type="obsolete">This is embarassing. I seem to be a bit lost</translation>
        </message>
    </context>
</TS>
